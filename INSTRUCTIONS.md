To replicate the results, please follow these detailed instructions. Note that we have not included the training data due to its large size, but we can provide it upon request.

### Prerequisites

1. **Install Google Cloud SDK:**
   - Download and install the [Google Cloud SDK](https://cloud.google.com/sdk/docs/install).
   - Initialize the SDK and authenticate using the provided key file:
     ```bash
     gcloud auth activate-service-account --key-file=<path-to-key-file.json>
     ```

2. **Download the Required Data:**
- From the root directory of your project, run the following command to download the necessary files from the Google Cloud Storage bucket:
  ```bash
  gsutil -m cp -r "gs://data-science-project/out/coad" "gs://data-science-project/out/predict" ./out
  gsutil -m cp -r "gs://data-science-project/postprocess" ./res
  ```

### Running the Scripts

1. **Evaluate Model Performance:**
   - The `evaluate.py` script will generate logs about model accuracy and AUC.
   - Run it first with the default settings:
     ```bash
     python evaluate.py
     ```
   - After the initial run, modify line 390 to `is_consensus = True` and run the script again to generate consensus scores:
     ```bash
     python evaluate.py
     ```

2. **Generate Molecular Cartographies:**
   - Next, run the `cartography.py` script, which relies on the results from `evaluate.py`.
   - This script creates molecular cartographies for the classes used in the code.
   - You can switch between different classes by commenting/uncommenting lines 93/94:
     ```bash
     python cartography.py
     ```

3. **Create Heterogeneity Maps:**
   - The `heterogeneity.py` script generates heterogeneity maps.
   - There are 6 class combinations to consider, given the 4 markers.
   - Running this script will calculate the heterogeneity score and add it to the clinical file:
     ```bash
     python heterogeneity.py
     ```
   - Note: The clinical file already contains these numbers, so re-running may not change the file content.

4. **Perform Survival Analysis:**
   - The survival analysis scripts are located in the `src/COAD_scripts/analysis` directory.
   - These scripts offer various options, detailed in their respective helpers.
   - To view the available options for `survival_analysis_by_HTI.py`, run:
     ```bash
     python src/COAD_scripts/analysis/survival_analysis_by_HTI.py -h
     ```
   - Similarly, for `evaluate_HTI_vs_expression.py`, run:
     ```bash
     python src/COAD_scripts/analysis/evaluate_HTI_vs_expression.py -h
     ```
   - These scripts will create survival graphs based on the selected options.

### Additional Information

- **Preprocessing and Training/Predicting Steps:**
  - The preprocessing and training/predicting steps are also contained in the `src` directory. However, since they are time-consuming, we are not expecting you to run them and are not providing the data or the exact commands.
  
- **HPC Scripts:**
  - There are two bash scripts used for HPC, located in `src/COAD_scripts/HPC_slurm_scripts`:
    - One script is for training and running evaluation of different models.
    - The other script is for preprocessing for predictions.

By following these steps, you should be able to replicate the code execution and generate the necessary outputs. If you need the training data, please let us know, and we can provide it.

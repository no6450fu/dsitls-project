import argparse
import os
from shutil import rmtree

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import use
from numpy import isnan

from survival_analysis_by_HTI import make_hti_colname_easy_to_read, sanitize_filepath, get_age_boundaries


def generate_stratified_sub_dataframes(clinical_df, stratify_by):
    clinical_sub_dfs = None
    if stratify_by.lower() == "sex" and 'Sex' not in clinical_df.columns:
        raise ValueError("The 'Sex' column is required for stratification but not found in the DataFrame.")
    elif stratify_by.lower() == "sex":
        clinical_sub_dfs = [clinical_df[clinical_df["Sex"] == "Female"],
                            clinical_df[clinical_df["Sex"] == "Male"]]

    elif stratify_by.lower().startswith("age"):
        if stratify_by[-1].isdigit():
            # number of age bins given in argument, so we use it.
            bins = int(stratify_by[-1])
            if bins == 2:
                # default case
                pass
            else:
                # calculate #bins-1 boundaries, based on age distribution
                boundaries = get_age_boundaries(clinical_df["Diagnosis Age"], bins - 1)
                clinical_sub_dfs = []
                last_bound = 0
                for bound in boundaries:
                    if not last_bound == max(boundaries):
                        this_bin_data = clinical_df[(clinical_df["Diagnosis Age"] >= last_bound) |
                                                    (clinical_df["Diagnosis Age"] < bound)].copy()
                        this_bin_data.columns.name = f"Age >= {last_bound} < {bound}"
                        clinical_sub_dfs.append(this_bin_data)
                        last_bound = bound
                this_bin_data = clinical_df[clinical_df["Diagnosis Age"] >= last_bound]
                this_bin_data.columns.name = f"Age >= {bound}"
                clinical_sub_dfs.append(this_bin_data)

        if not clinical_sub_dfs:
            # above derivation of bins did not work, so we split into two bins with the median as the boundary
            age_median = int(clinical_df["Diagnosis Age"].median())
            bin1_data = clinical_df[
                (clinical_df['Diagnosis Age'] >= 0) | (clinical_df['Diagnosis Age'] < age_median)].copy()
            if not bin1_data.empty:
                bin1_data.columns.name = f"Age >= 0 < {age_median}"
            bin2_data = clinical_df[clinical_df['Diagnosis Age'] >= age_median].copy()
            if not bin2_data.empty:
                bin2_data.columns.name = f"Age >= {age_median}"
            if not bin1_data.empty and not bin2_data.empty:
                clinical_sub_dfs = [bin1_data, bin2_data]
    return clinical_sub_dfs


def split_hti_by_expression_value(clinical_df, marker, col):
    # sort HTI values by whether or not gene expression values are above or
    # below the respective median expression rate
    greater_than_median_htis = []
    smaller_equal_median_htis = []
    nan_counter = 0

    # find column holding categorization string "lo", or "hi", regarding the median
    marker_column = [this_col for this_col in clinical_df.columns if
                     marker.lower().strip() in this_col.lower().strip() and this_col.endswith(
                         "_50_pctl")]

    # generate sub dataframe holding categorization string and HTI score
    clinical_df_subset = clinical_df.loc[:, [col.strip(), marker_column[0].strip()]]

    # sort HTI scores into two groups
    for index, row in clinical_df_subset.iterrows():
        if row[marker_column].values[0] == "hi":
            greater_than_median_htis.append(row[col])
        elif row[marker_column].values[0] == "lo":
            smaller_equal_median_htis.append(row[col])
        elif isnan(row[marker_column].values[0]):
            nan_counter += 1
        else:
            print(f"Error. value: {row[marker_column].values[0]}")

    # delete nan values and check if there is enough to work with
    greater_than_median_htis = [x for x in greater_than_median_htis if not isnan(x)]
    smaller_equal_median_htis = [x for x in smaller_equal_median_htis if not isnan(x)]

    return smaller_equal_median_htis, greater_than_median_htis


def check_hti_was_splitable(greater_than_median_htis, smaller_equal_median_htis, marker, col):
    if not greater_than_median_htis or not smaller_equal_median_htis:
        if not greater_than_median_htis:
            print(
                f"Found no HTI scores with respective expression value greater than median"
                f" for {col} and {marker}.")
        else:
            print(
                f"Found no HTI scores with respective expression value smaller or equal than median"
                f" for {col} and {marker}.")
        return False

    if len(greater_than_median_htis) < 5:
        print(f"Warning, only {len(greater_than_median_htis)} "
              f"datapoints in greater_than_median_htis for {col}")
    if len(smaller_equal_median_htis) < 5:
        print(f"Warning, only {len(smaller_equal_median_htis)} "
              f"datapoints in smaller_than_median_htis for {col}")
    return True


def plot_heterogeneity_boxplots(clincial_csv_file_path, stratify_by=None, outpath=None):
    """
    Creates boxplot from two groups: HTI scores with an associated underlying gene expression value of more or less
    than the median expression of that gene.
    @param clincial_csv_file_path: path to clinical.csv, clinical data file holding heterogeneity index scores and
    categorization string for expression compared to median.
    @param stratify_by: Subgroup feature to stratify by. Either \"Sex\" or \"Age_n\", where n is the number of
    age bins (2 by default, split by median)
    @param outpath: Optional. Path where to save plots to. If none is provided, the plots will be shown.
    """
    # Read the CSV file into a DataFrame
    clinical_df = pd.read_csv(clincial_csv_file_path, skipinitialspace=True)
    clinical_sub_dfs = [clinical_df]

    # Identify columns that contain the heterogeneity scores
    heterogeneity_columns = [col for col in clinical_df.columns if
                             'heterogeneity' in col.lower() and not "baseline" in col.lower()]

    if stratify_by:
        clinical_sub_dfs = None
        clinical_sub_dfs = generate_stratified_sub_dataframes(clinical_df, stratify_by)

    if clinical_sub_dfs:
        # make sure data is splitable (esp. for age with many bins)
        for clinical_df in clinical_sub_dfs:
            # Plot 2 boxplots per heterogeneity column: 1 per marker involved
            for col in heterogeneity_columns:
                if "baseline" in col:
                    # only HTI scores generated from interaction of two features
                    continue

                # extract biomarker gene names from column name
                title = make_hti_colname_easy_to_read(col)
                marker_1 = title.split(" and ")[0]
                marker_2 = title.split(" and ")[1]

                # initialize fresh plot
                fig, axs = plt.subplots(2, 1, figsize=(8, 12))
                markers = [marker_1, marker_2]
                for marker_index, marker in enumerate(markers):
                    smaller_equal_median_htis, greater_than_median_htis = split_hti_by_expression_value(clinical_df,
                                                                                                        marker, col)
                    good_to_go = check_hti_was_splitable(greater_than_median_htis, smaller_equal_median_htis, marker,
                                                         col)
                    if not good_to_go:
                        plt.close()
                        break

                    # begin with boxplot, 1 sub plot per expressed gene from which HTI was generated with
                    ax = axs[marker_index]
                    box = ax.boxplot([smaller_equal_median_htis, greater_than_median_htis],
                                     tick_labels=["≤ median", "> median"],
                                     widths=[0.9, 0.9],
                                     patch_artist=True, showfliers=False)

                    # Change the color of the boxes
                    colors = ['#B39DDB', '#6A3D9A']
                    for patch, color in zip(box['boxes'], colors):
                        patch.set_facecolor(color)
                    for median in box['medians']:
                        median.set_color('black')

                    ax.set_xlabel(marker)
                    ax.set_ylabel(f"HT-Index for {title}")

                    if stratify_by.lower() == "sex":
                        if 'Sex' not in clinical_df.columns:
                            raise ValueError("The DataFrame does not contain the column 'Sex' for stratification.")
                        else:
                            filename = "_".join([title, clinical_df["Sex"].unique()[0], "boxplot.png"])
                            ax.set_title(f'Stratified by Sex: {clinical_df["Sex"].unique()[0]}')
                    elif stratify_by.lower().startswith("age"):
                        filename = "_".join([title, clinical_df.columns.name, "boxplot.png"])
                        ax.set_title(f'Stratified by age: {clinical_df.columns.name}')

                    else:
                        filename = "_".join([title, "boxplot.png"])

                    if marker_index == 1:
                        # there can not be more than two markers, so we stop
                        plt.tight_layout()
                        if outpath:
                            filename = sanitize_filepath(filename)
                            filepath = os.path.join(outpath, filename)
                            plt.savefig(filepath)
                            print("Saved plot at: ", filepath)
                            plt.close()
                        else:
                            plt.show()


def main(clinical_path, stratify_by=None, outpath=None):
    """
    @param clinical_path: clinical.csv, clinical data file holding survival information and columns holding
    the heterogeneity values
    @param stratify_by: Subgroup feature to stratify by. Either \"Sex\" or \"Age_n\", where n is the number of
    age bins (2 by default, split by median)
    @param outpath: Optional. Path where to save plots to. If none is provided, the plots will be shown.
    """
    if os.name == "nt":
        # Windows needs to
        use('TkAgg')

    if outpath:
        if os.path.exists(outpath):
            if stratify_by.lower() == "sex":
                outpath = outpath + "_stratified_sex"
            if stratify_by.lower().startswith("age"):
                outpath = outpath + "_stratified_age"
                if os.path.exists(outpath) and stratify_by[-1].isdigit():
                    outpath = outpath + f"_{stratify_by[-1]}"
            if os.path.exists(outpath):
                rmtree(outpath)

    if outpath and not os.path.exists(outpath):
        os.makedirs(outpath)

    plot_heterogeneity_boxplots(clinical_path, stratify_by, outpath)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Kaplan-Meier Survival Analysis")
    parser.add_argument("--clinical_csv", type=str,
                        default='../../../res/merged_clinical_mirna_data_test.csv',
                        help="Path to the CSV file containing the data.")
    parser.add_argument("--stratify_by", type=str, nargs='+',
                        # default=None,
                        # default="Sex",
                        default="Age",
                        # default="Age_3",
                        help="Subgroup feature to stratify by. Either \"Sex\" or \"Age_n\", where n is the number of "
                             "age bins (2 by default, split by median)")
    parser.add_argument("--outpath", type=str,
                        default="../../../out/hti_vs_expression_plots",
                        # default=None,
                        help="Path to the output directory for plots.")
    args = parser.parse_args()

    main(args.clinical_csv, stratify_by=args.stratify_by, outpath=args.outpath)

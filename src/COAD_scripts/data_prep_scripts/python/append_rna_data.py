import pandas as pd

# appends expression data column for specified miRNA gene to clinical data table
base_dir = "../../../../res/"

clinical_data_file = base_dir + 'clinical_data_coadread_tcga.tsv'
clinical_data_df = pd.read_csv(clinical_data_file, sep='\t')

mirna_data_file = base_dir + 'compiled_mirnas_quantification.csv'
mirna_data_df = pd.read_csv(mirna_data_file)

gene_name = 'hsa-mir-143'

gene_df = mirna_data_df[mirna_data_df['miRNA_ID'] == gene_name]
gene_df = gene_df.T.reset_index()

gene_df.columns = gene_df.iloc[0]
gene_df = gene_df[1:]
gene_df.rename(columns={'miRNA_ID': 'Sample ID', gene_name: gene_name}, inplace=True)
gene_df['Patient ID'] = [x[:12] for x in gene_df['Sample ID'].to_list()]
merged_df = pd.merge(clinical_data_df, gene_df[['Patient ID', gene_name]], left_on='Patient ID', right_on='Patient ID',
                     how='left')

output_file = base_dir + 'merged_clinical_mirna_data.csv'
merged_df.to_csv(output_file, index=False)

import os
import pandas as pd

# create symbolic links to .svs image slides in a given directory and it's subdirectories, if the id
# (filename or dirname) of a .svs file is present in given csv file at 'file_path'


# Read the manifest file into a DataFrame
# file_path = "../../../../notes/TCGA_COAD_svs_quality_assesment/mediocre_quality_sort_out_maybe.txt"
file_path = "../../notes/TCGA_COAD_svs_quality_assesment/bad_quality_sort_out_yes.txt"
df = pd.read_csv(file_path, sep='\t')

# Get the list of subfolder IDs
subfolder_ids = df['id'].tolist()

# Define the source directory containing all subfolders and the target directory for accessible subfolders
source_directory = '../../../../../../data/slides/diagnostic/'
# target_directory = '../../../../../../data/slides/diagnostic/svs_links_maybe'
target_directory = '../../../../../../data/slides/diagnostic/svs_links_yes'

# Create the target directory if it does not exist
os.makedirs(target_directory, exist_ok=True)

# Create symbolic links for each subfolder ID
for subfolder_id in subfolder_ids:
    source_dir_path = os.path.join(source_directory, subfolder_id)
    target_path = os.path.join(target_directory, subfolder_id)
    # target_path = target_directory
    print(target_path)
    # Check if the source path exists before creating the symlink
    if os.path.exists(source_dir_path):
        [os.symlink(os.path.join(source_dir_path, file), target_path) for file in os.listdir(source_dir_path) if
         file.endswith('.svs')]
    else:
        print(f"Source path does not exist: {source_dir_path}")

print("Symbolic links created successfully.")
